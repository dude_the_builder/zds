const std = @import("std");

pub fn build(b: *std.Build) void {
    const target = b.standardTargetOptions(.{});

    _ = b.addModule("zds", .{
        .root_source_file = b.path("src/zds.zig"),
        .target = target,
        .optimize = .ReleaseSmall,
    });
}
