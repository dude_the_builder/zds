const std = @import("std");

pub fn build(b: *std.Build) void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    // Sample exe.
    const exe = b.addExecutable(.{
        .name = "main",
        .root_source_file = b.path("src/main.zig"),
        .target = target,
        .optimize = optimize,
    });
    b.installArtifact(exe);

    // Sample docs gen and docs web server (zds) build.
    // This sets up an `-Ddocs` command line option to
    // `zig build` that triggers docs generation and zds
    // build. After this, you'll find a `docs` subdirectory
    // in the installation prefix containing the auto-
    // generated docs and the `zds` executable to run the
    // web server for the docs. Change to that subdirectory
    // and then run the server: `cd zig-out/docs && ./zds`.
    docsGen(b, exe);
}

fn docsGen(b: *std.Build, exe: *std.Build.Step.Compile) void {
    const docs_wf = b.addWriteFiles();
    _ = docs_wf.addCopyDirectory(exe.getEmittedDocs(), "docs", .{});

    const zds_wf = b.addWriteFiles();
    const zds_src = zds_wf.add("zds.zig",
        \\const std = @import("std");
        \\
        \\pub fn main() !void {
        \\    try @import("zds").zds("127.0.0.1", 8080, 6);
        \\}
    );

    const zds = b.dependency("zds", .{});

    const docs_server = b.addExecutable(.{
        .name = "zds",
        .root_source_file = zds_src,
        .target = b.host,
        .optimize = .ReleaseSmall,
    });
    docs_server.root_module.addImport("zds", zds.module("zds"));

    _ = docs_wf.addCopyFile(docs_server.getEmittedBin(), "docs/zds");

    const install_docs = b.addInstallDirectory(.{
        .source_dir = docs_wf.getDirectory(),
        .install_dir = .prefix,
        .install_subdir = ".",
    });

    const docs_opt = b.option(bool, "docs", "Generate and install docs.") orelse false;
    if (docs_opt) b.getInstallStep().dependOn(&install_docs.step);
}
