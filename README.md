# zds

## This Project Needs a New Maintainer
If you would like to take over the development and maintenace of this project,
let me know by opening an issue.

A *just-enough-functionality* web server for auto-generated Zig documentation.

To see an example of how to use this in a project, see the sample `build.zig` file
in the [sample](sample) directory. In that sample, once you run `zig build -Ddocs`,
you'll have a `docs` subdirectory in your installation prefix (`zig-out` by default).
Then you can change into that directory and run the server with:

```plain
$ cd zig-out/docs && ./zds
```

